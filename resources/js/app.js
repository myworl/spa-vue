require('./bootstrap');

import { createApp } from 'vue'
import router from "./router"
import App from './components/App'
import axios from 'axios'


const app = createApp(App)
app.config.globalProperties.$axios = axios;
app.use(router)
app.mount('#app')
