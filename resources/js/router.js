import { createRouter, createWebHistory } from 'vue-router';

import Home from './components/Home';
import About from './components/About';
import Desk from './components/desks/Desk';
import ShowDesk from './components/desks/ShowDesk';

const routes = [
    {
        path:'/',
        name: 'home',
        component: Home
    },
    {
        path:'/about',
        name: 'about',
        component: About
    },
    {
        path:'/desks',
        name: 'desks',
        component: Desk
    },
    {
        path:'/desks/:deskId',
        name: 'showDesks',
        component: ShowDesk,
        props: true
    }
];
const router = createRouter({
    history: createWebHistory(),
    routes: routes,
});
export  default router;
