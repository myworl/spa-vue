<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DeskController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::controller(DeskController::class)->group(function () {
    Route::get('/desks',  'index');
    Route::get('/desks/{show_id}',  'show');
    Route::put('/desks/{show_id}',  'update');
    Route::post('/desks/',  'create');
    Route::delete('/desks/{show_id}',  'delete');
});
