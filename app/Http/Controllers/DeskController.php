<?php

namespace App\Http\Controllers;

use App\Models\Desk;
use http\Client\Response;
use Illuminate\Http\Request;

class DeskController extends Controller
{
    public function index()
    {
        return  Desk::all();
    }

    public function show(Desk $show_id)
    {
        return  $show_id;
    }

    public function update(Request $request, Desk $show_id)
    {
        $show_id->name = $request->get('name');
        $show_id->save();
        return  $show_id;
    }

    public function delete(Desk $show_id)
    {
          $show_id->delete();
          return response(['massage'=>'Запись удалена'], 200);
    }

    public function create(Request $request ){

        $desk_db =  new Desk();
        $desk_db -> name = $request->get('name');
        $desk_db -> discription = 22332;
        $desk_db->save();
        return $desk_db;
    }
}
